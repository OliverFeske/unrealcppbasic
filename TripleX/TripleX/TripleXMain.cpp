#include <iostream>
#include<ctime>

using namespace std;

bool PlayGame(int Difficulty)
{
	cout << "You are a secret agent breaking into a Level " << Difficulty << " secure server room.\n";
	cout << "You need to enter the correct codes to continue...\n\n";

	const int CodeA = rand() % Difficulty + Difficulty;
	const int CodeB = rand() % Difficulty + Difficulty;
	const int CodeC = rand() % Difficulty + Difficulty;

	const int CodeSum = CodeA + CodeB + CodeC;
	const int CodeProduct = CodeA * CodeB * CodeC;
	cout << "- There are 3 numbers in the code.\n";
	cout << "- The code adds up to: " << CodeSum;
	cout << "\n- The code multiplies to: " << CodeProduct << "\n\n";

	// playerinput management
	int GuessA, GuessB, GuessC;
	cin >> GuessA >> GuessB >> GuessC; // input gets seperated by space

	int GuessSum = GuessA + GuessB + GuessC;
	int GuessProduct = GuessA * GuessB * GuessC;

	if (GuessSum == CodeSum && GuessProduct == CodeProduct)
	{
		cout << "---Well done agent! You have successfully extracted a file! Keep going!---\n\n";
		return true;
	}
	else
	{
		cout << "---You entered the wrong code! Careful agent! Try again!---\n\n";
		return false;
	}
}

int main()
{
	srand(time(NULL));

	int LevelDifficulty = 1;
	const int MaxDifficulty = 5;

	while (LevelDifficulty <= MaxDifficulty) 
	{
		bool bLevelComplete = PlayGame(LevelDifficulty);
		cin.clear(); // clears errors
		cin.ignore(); // discards the buffer left in the input

		if (bLevelComplete)
		{
			LevelDifficulty++;
		}
	}

	cout << "\n---Great work agent! You got all the files! Now get out of there!---\n";

	return 0;
}